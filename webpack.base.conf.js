const path = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const { CleanWebpackPlugin } = require("clean-webpack-plugin")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const ESLintPlugin = require("eslint-webpack-plugin")
const devMode = process.env.NODE_ENV !== "production"
const STATIC_ROOT_DIR = path.resolve(__dirname, "./src")

const lessRegex = /\.less$/

const lessModuleRegex = /\.module\.less$/

module.exports = {
  entry: {
    app: path.resolve(__dirname, "./src/index.js")
  },

  resolve: {
    alias: {
      "@global": path.resolve(STATIC_ROOT_DIR, "@global")
    }
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: ["babel-loader"],
        exclude: /node_modules/
      },
      {
        test: /\.css|scss$/,
        exclude: /node_modules/,
        use: [
          devMode ? "style-loader" : MiniCssExtractPlugin.loader,
          // css modules
          "css-loader?modules",
          "postcss-loader",
          {
            loader: "sass-loader",
            options: {
              // 附加内容
              additionalData: `@charset 'utf-8';@import "@global/scss/core.scss";`
            }
          }
        ]
      },
      {
        test: /\.css$/,
        include: /node_modules/,
        use: [
          devMode ? "style-loader" : MiniCssExtractPlugin.loader,
          // no css modules
          "css-loader"
          // "postcss-loader",
          // "sass-loader"
        ]
      },
      {
        test: /\.less/,
        use: [
          "style-loader",
          "css-loader",
          {
            loader: "less-loader",
            options: {
              lessOptions: {
                javascriptEnabled: true
              }
            }
          }
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ["file-loader"]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ["file-loader"]
      }
    ]
  },

  plugins: [
    new CleanWebpackPlugin(),

    new HtmlWebpackPlugin({
      filename: "index.html",
      template: path.resolve(__dirname, "./public/template.html")
    }),

    new ESLintPlugin({ fix: true })
  ]
}
