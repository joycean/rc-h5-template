const webpack = require("webpack");

const config = require("./webpack.prod.conf.js");

const compiler = webpack(config, function (err, stats) {
  if (err) {
    throw err
  }
  const info = stats.toJson();

  if (stats.hasErrors()) {
    console.error(`Build failed with errors.\n`, info.errors)
    process.exit(1)
  }
});
