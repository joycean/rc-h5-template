const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const webpack = require("webpack");
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");
const merge = require("webpack-merge");
const baseConfig = require("./webpack.base.conf");

const isAnalyze =
  process.argv.includes("--analyze") || process.argv.includes("--analyse");

module.exports = merge(baseConfig, {
  mode: "development",
  devtool: "inline-source-map",
  //   devServer: {
  //       contentBase: './dist',
  //       hot: true
  //   },

  output: {
    filename: `[name].js`,
    path: path.resolve(__dirname, "dist"),
    publicPath: "/"
  },

  plugins: [
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify("development")
    }),
    new webpack.HotModuleReplacementPlugin(),

    // Webpack Bundle Analyzer
    // https://github.com/th0r/webpack-bundle-analyzer
    ...(isAnalyze ? [new BundleAnalyzerPlugin()] : [])
  ]
});
