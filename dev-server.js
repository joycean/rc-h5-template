const webpackDevServer = require("webpack-dev-server");
const webpack = require("webpack");

const config = require("./webpack.dev.conf.js");

// devServer配置
const options = {
  contentBase: "./dist",
  hot: true,
  host: "localhost",
  historyApiFallback: true,
  useLocalIp: true,
  // proxy代理
  // https://www.webpackjs.com/configuration/dev-server/#devserver-proxy
  proxy: [
    {
      context: ["/atsng", "/api"],
      target: "https://www.ifchange.com",
      secure: false,
      headers: {
        Cookie:
					"IFCHANGE_TOB=g6j6rgi6oqn1553v1atjcp42u3;SERVERID=;tob_csrf_token=5dbfa0b92fad9c4677f1c0286d200008",
        "X-XSRF-TOKEN": "5dbfa0b92fad9c4677f1c0286d200008"
      }
    }
  ]
};

webpackDevServer.addDevServerEntrypoints(config, options);
const compiler = webpack(config);
const server = new webpackDevServer(compiler, options);

server.listen(5000, "0.0.0.0", () => {
  console.log("dev server listening on port 5000");
});
