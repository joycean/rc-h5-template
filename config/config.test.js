const customize = process.env.CUSTOMIZE_NAME;
const env = process.env.EGG_SERVER_ENV;

exports.logger = {
  // 日志
  dir: `/opt/log/${customize}_${env}`
};
