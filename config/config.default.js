/* eslint valid-jsdoc: "off" */

"use strict";

const path = require("path");
/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = (appInfo) => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = (exports = {});

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + "_1606045624482_3070";

  // add your middleware config here
  config.middleware = [];

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };

  // 静态资源配置
  config.static = {
    prefix: "/static",
    dir: path.join(appInfo.baseDir, "public/dist")
  };

  // 模板配置
  config.view = {
    root: path.join(appInfo.baseDir, "public/dist"),
    defaultViewEngine: "nunjucks",
    mapping: {
      ".html": "nunjucks" //左边写成.html后缀，会自动渲染.html文件
    }
  };


  return {
    ...config,
    ...userConfig
  };
};
