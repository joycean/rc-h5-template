import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { hot } from "react-hot-loader/root";
import loadable from "@loadable/component";
import URI from 'urijs';
import "./index.scss";
import App from "./App";

const {token} = URI().search(true) || {};

function Router() {
  return (
    <BrowserRouter>
      <App />
    </BrowserRouter>
  );
}
const HMRRouter = hot(Router);

ReactDOM.render(<HMRRouter />, document.getElementById("root"));

// if (module.hot) {
//   module.hot.accept('./print.js', function() {
//     console.log('Accepting the updated printMe module!');
//     document.body.removeChild(element);
//     element = component(); // Re-render the "component" to update the click handler
//     document.body.appendChild(element);
//   })
// }
