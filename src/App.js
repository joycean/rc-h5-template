import React, { useState } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import loadable from "@loadable/component";

// loadable 动态导入实现Code Splitting
// https://reactrouter.com/web/guides/code-splitting
const Index = loadable(() => import("./pages/index"));

export default function App() {
  return (
    <Switch>
      <Route path="/" exact component={Index} />
    </Switch>
  );
}
