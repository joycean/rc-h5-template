import axios from "axios";
import { assign, get } from "lodash";
import utils from "./utils";

const CancelToken = axios.CancelToken;

export default function fetch(url, options) {
  options = options || {};

  options.url = url;
  options.method = (options.method || "get").toUpperCase();
  options.dataType = options.dataType || "json";
  options.jsonify = options.jsonify !== false;
  options.headers = options.headers || {};

  let onComplete = options.onComplete;
  if (options.method === "POST") {
    let data = options.data || {};
    // api开头的接口只能使用 form-data 的方式发送数据
    if (/^\/api\//.test(url)) {
      options.jsonify = false;
    }
    if (options.jsonify !== true) {
      options.data = utils.params(data);
      options.headers["Content-Type"] = "application/x-www-form-urlencoded";
    } else {
      options.headers["content-type"] = "application/json";
      options.data = JSON.stringify(data);
    }
  } else if (options.method === "GET") {
    let params = options.params || {};
    options.params = params;
  }

  const source = CancelToken.source();
  options.cancelToken = source.token;

  let axiosPromise = axios(options)
    .then(async function (response) {
      if (response.status >= 200 && response.status < 300) {
        let data = response.data;
        if (
          data.err_no * 1 ||
					(data.err_no !== "0" && data.err_no && data.err_no.length)
        ) {
          let err = new Error(data.err_msg);
          err.response = data;
          // reject(err);
          return [err, null];
        }
        data.headers = response.headers;
        utils.isFunction(onComplete) && onComplete(null, data);
        // resolve(data);
        return [null, data];
      }
      let error = new Error("系统繁忙，请稍后重试！");
      error.response = response;
      // throw error;
      return [error, null];
    })
    .catch(function (error) {
      const errorProto = get(error, "constructor.prototype") || {};
      // 标识是 cancel 的报错，需要在各个业务中处理
      if (errorProto.__CANCEL__) {
        error.cancel = true;
      }
      utils.isFunction(onComplete) && onComplete(error);
      // throw error;
      return [error, null];
    });
  axiosPromise.cancel = function (msg) {
    source.cancel(msg);
    return this;
  };
  return axiosPromise;
}

// fetch post 快捷方法
fetch.post = function (url, data, options) {
  return fetch(
    url,
    assign(options || {}, {
      method: "POST",
      data: data
    })
  );
};

// fetch get 快捷方法
fetch.get = function (url, params, options) {
  return fetch(
    url,
    assign(options || {}, {
      method: "GET",
      params: params
    })
  );
};
