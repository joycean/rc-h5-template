'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index() {
    const { ctx } = this;
    // TODO 判断登陆态  获取用户信息
    await ctx.render('index', {});
  }
}

module.exports = HomeController;
