const pkg = require("./package.json");

module.exports = {
  // parser: 'sugarss',
  plugins: [
    // css自动补齐，浏览器兼容
    require("autoprefixer")({
      overrideBrowserslist: [
        ">1%",
        "last 4 versions",
        "Firefox ESR",
        "not ie < 9",
        "iOS >= 8",
        "Android >= 4"
      ],
      flexbox: "no-2009"
    }),

    // Convert pixel units to rem (root em) units using PostCSS
    // https://github.com/cuth/postcss-pxtorem
    require("postcss-pxtorem")({
      rootValue: 75, // 即 75px为1rem,
      propList: ["*"] // 需要把px转为rem的属性，*代表所有，
    })
  ]
};
