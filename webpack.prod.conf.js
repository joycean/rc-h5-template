const path = require("path");
const merge = require("webpack-merge");
const baseConfig = require("./webpack.base.conf");
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require("webpack");

const isAnalyze =
  process.argv.includes("--analyze") || process.argv.includes("--analyse");

module.exports = merge(baseConfig, {
  mode: "production",
  //   devtool: "source-map",

  output: {
    filename: `[name].[chunkhash:7].js`,
    path: path.resolve(__dirname, "public/dist"),
    publicPath: "/static/"
  },

  optimization: {
    splitChunks: {
      chunks: "all", // 代码分割时对异步代码生效，all：所有代码有效，inital：同步代码有效 async: 异步代码有效
      minSize: 30000, // 代码分割最小的模块大小，引入的模块大于 30000B 才做代码分割
      // maxSize: 0, // 代码分割最大的模块大小，大于这个值要进行代码分割，一般使用默认值
      minChunks: 1, // 引入的次数大于等于1时才进行代码分割
      maxAsyncRequests: 5, // 最大的异步请求数量,也就是同时加载的模块最大模块数量
      maxInitialRequests: 3, // 入口文件做代码分割最多分成 x 个 js 文件
      automaticNameDelimiter: "~", // 文件生成时的连接符
      // automaticNameMaxLength: 30, // 自动生成的文件名的最大长度
      cacheGroups: {
        // "react-vendor": {
        //   test: (module, chunks) => /react/.test(module.context),
        //   priority: 1,
        // },
        vendors: {
          test: /[\\/]node_modules[\\/]/, // 位于node_modules中的模块做代码分割
          priority: -10 // 根据优先级决定打包到哪个组里，例如一个 node_modules 中的模块进行代码
        }, // 分割，，既满足 vendors，又满足 default，那么根据优先级会打包到 vendors 组中。
        default: {
          // 没有 test 表明所有的模块都能进入 default 组，但是注意它的优先级较低。
          priority: -20, //  根据优先级决定打包到哪个组里,打包到优先级高的组里。
          reuseExistingChunk: true // //如果一个模块已经被打包过了,那么再打包时就忽略这个上模块
        }
      }
    }
  },

  module: {
    rules: [
      // 
    ]
  },

  plugins: [
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify("production")
    }),


    new MiniCssExtractPlugin({
      filename: '[name].[chunkhash:7].css',
      chunkFilename: '[id].[chunkhash:7].css'
    }),

    // Webpack Bundle Analyzer
    // https://github.com/th0r/webpack-bundle-analyzer
    ...(isAnalyze ? [new BundleAnalyzerPlugin()] : [])
  ]
});
